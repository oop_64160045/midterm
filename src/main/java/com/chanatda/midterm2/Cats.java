package com.chanatda.midterm2;

public class Cats {
    private String Name;
    private String Breed;
    private String Size;
    private int Status;

    public Cats(String name, String breed, String size) {
        this.Name = name;
        this.Breed = breed;
        this.Size = size;
        this.Status = 0;
    }

    public void printStatusCats() {
        if (Status == 0) {
            System.out.println("Name : " + Name + " Breed : " + Breed + " Size : " + Size + "\nStatus : Cat is eat food");

        }
        if (Status == 1) {
            System.out.println("Name : " + Name + " Breed : " + Breed + " Size : " + Size + "\nStatus : Cat sleep");
        }
    }

    public void CatsIdle() {
        Status = 0;
        printStatusCats();
    }

    public void CatsBusy() {
        Status = 1;
        printStatusCats();
    }
}
