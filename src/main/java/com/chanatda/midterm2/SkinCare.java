package com.chanatda.midterm2;

public class SkinCare {
    private String name;
    private String properties;
    private int status;

    public SkinCare(String name, String properties, int status) {
        this.name = name;
        this.properties = properties;
        this.status = status;
    }

    public void printSkinCare() {
        if (status == 1) {
            System.out.println("Name : " + name + " Properties : " + properties + "\nApply To Face");

        }
        if (status == 2) {
            System.out.println("Name : " + name + " Properties : " + properties + "\nApply To Body");

        }
        if (status == 3) {
            System.out.println("Name : " + name + " Properties : " + properties + "\nApply To Face and Body");

        }

    }

    public void Face() {
        if (status == 1) {
            printSkinCare();

        } else {
            System.out.println("Can't Apply To Face");
        }

    }

    public void Body() {
        if (status == 2) {
            printSkinCare();

        } else {
            System.out.println("Can't Apply To Body");
        }
    }

    public void FaceAndBody() {
        if (status == 3) {
            printSkinCare();

        } else {
            System.out.println("Can't Apply To Face and Body");
        }

    }

}
