package com.chanatda.midterm2;

public class SkinCareApp {
    public static void main(String[] args) {
        SkinCare s1 = new SkinCare("Cleanser", "Clean On Face", 1);
        SkinCare s2 = new SkinCare("Moisturizer", "Retain Moisture In The Skin", 3);
        SkinCare s3 = new SkinCare("Toner", "Balance The Skin", 1);
        SkinCare s4 = new SkinCare("Lotion", "Provides Misture To The Skin", 2);
        SkinCare s5 = new SkinCare("Sunscreen", "Protect Skin From Sunlight", 3);

        s1.Face();
        s1.Body();
        s1.FaceAndBody();
        System.out.println();

        s2.Face();
        s2.Body();
        s2.FaceAndBody();
        System.out.println();

        s3.Face();
        s3.Body();
        s3.FaceAndBody();
        System.out.println();

        s4.Face();
        s4.Body();
        s4.FaceAndBody();
        System.out.println();

        s5.Face();
        s5.Body();
        s5.FaceAndBody();
        System.out.println();
    }
}
